from pathlib import Path
import os
import re
import asyncio
import datetime
from vpn_checker import VPN_Checker
from database import DataBase
import logging
import requests

from dotenv import load_dotenv
load_dotenv()

from log_set import init_logger

logger = init_logger()

async def get_all_ss_files_list(root_folder):
    file_list = set()
    struct_data = re.compile(r"ss:\/\/(.*?)@((?:[0-9]{1,3}\.){3}[0-9]{1,3}):(\d*?)#(.*?)$")
    for path, subdirs, files in os.walk(root_folder):
        for file_name in files:
            full_file_path = Path(path, file_name)
            with open(full_file_path, 'r') as file_ss:
                in_file_text = file_ss.read(300)
            if struct_data.match(in_file_text):
                file_list.add("{0}".format(full_file_path))
    return file_list

async def get_all_ovpn_files_list(root_folder, file_name_folder_wik_keys="server-conf.txt"):
    """
    функция перебирает все файлы в указанной директории и ее дочерних
    если в папке есть файл .ovpn то используем его
    если такого файла нет то добавляем директорию, в которой должны быть:
    - server-conf.txt
    - client1.key
    - client1.crt
    - ca.crt
    :param root_folder: рут папка где в подпапках есть либо папки с ключами и коф файлами либо один opvp файл
    file_name_folder_wik_keys: имя файла, которій должен біть в папке. в нем конфигурация ovpn и подразумевается
    что все ключи лежат рядом с именами
    - client1.key
    - client1.crt
    - ca.crt
    :return:
        перечень файлов и путей
    """
    file_list = set()
    for path, subdirs, files in os.walk(root_folder):
        if file_name_folder_wik_keys in files:
            req_file_list = ['client1.crt', 'client1.key', 'ca.crt']
            break_flg = False
            # проверяем чтобы все необходимые файлы были в директории и именно с такими названиями
            for file_name in req_file_list:
                if file_name not in files:
                    break_flg = True
            if not break_flg:
                file_list.add("{0}".format(Path(path)))
        for file_name in files:
            if file_name.endswith((".ovpn",)):
                full_file_path = Path(path, file_name)
                if not full_file_path.is_absolute():
                    full_file_path = Path(Path.cwd(), full_file_path)
                file_list.add("{0}".format(full_file_path))
    return file_list


async def periodic1_timer(timer=5, db=None):
    print("timer1 task run")
    ovpn_files_folder = os.getenv("OVPN_FILES_ROOT_FOLDER")
    ss_files_folder = os.getenv("SS_FILES_ROOT_FOLDER")
    # время чтоб все стартонуть успело
    await asyncio.sleep(2)
    while True:
        await asyncio.sleep(timer)
        try:
            logger.debug("run check files in folders")
            await main(db, 'ovpn', ovpn_files_root_folder= ovpn_files_folder)
            await main(db, 'ss', ss_file_root_folder=ss_files_folder)
        except Exception as ex:
            logger.exception("Error in get file list")


async def periodic2_timer(timer=3 * 60 * 60, db=None):
    print("timer2 task run")
    # время чтоб все стартонуть успело
    await asyncio.sleep(2)
    while True:
        try:
            await asyncio.sleep(timer)
            logger.debug("run check vpn")
            await clear_db_old_rec(db, limit=240) # запуск 8 раз в сутки * 30
            await vpn_checker(db)
            await ss_checker(db)
        except Exception as ex:
            logger.exception("Error in run checker")

async def vpn_checker(db, periodic_task_row_id=None, file_ids=[]):
    logger.info("получаем список ip для исключения из проверки")
    exclude_list_ip = os.getenv('EXCLUDE_LIST_IP')
    exclede_ip_check = await get_exclude_list(exclude_list_ip)
    logger.debug(", ".join(exclede_ip_check))
    su_pass = os.getenv('SUPER_ROOT_PASSWORD')

    if not periodic_task_row_id:
        sql= "INSERT INTO periodic_task (name, date_start) values(?,?);"
        '''YYYY-MM-DD HH:MM:SS'''
        dt1 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        param = ("CHK_VPN", dt1)
        periodic_task_row_id = await db.execute(sql, parameters=param, commit=True)

    vpn_ckr = VPN_Checker(password=su_pass, db=db, logger=logger)
    if file_ids:
        sql = """
             select id, full_path, type_files from config_files cf 
             where id in ({0})
        """.format(",".join([str(id) for id in file_ids]))
        sql_del = "DELETE FROM validated where link_periodic_task_id = {0} and link_config_files_id in ({1});"
        sql_del = sql_del.format(periodic_task_row_id, ",".join([str(id) for id in file_ids]))
        res = await db.execute(sql_del, commit=True)
        dd = 0
    else:
        sql = "select id, full_path, type_files from config_files cf where delete_flg =false and (type_files='CONFIG' OR type_files='OVPN')"
    ovpn_files = await db.execute(sql, fetchall=True)
    for file in ovpn_files:
        try:
            if check_ip_in_exclude_list(file[1], exclede_ip_check):
                logger.info(f"пропускаю файл {file[1]} потому что его IP в списке исключений")
                result = {"result": True, "reasone": "в спсике исключений"}
            else:
                result = await vpn_ckr.check_openvpn(file[1], file[2])
        except Exception as ex:
            result = result = {"result": False, "reasone": f"{ex}"}
        sql = "INSERT INTO validated (link_config_files_id, link_periodic_task_id, date_exec, result_validate, reasone_error) VALUES(?,?,?,?,?)"
        dt2 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        param = (file[0], periodic_task_row_id, dt2, result["result"], result["reasone"])
        validated_row_id = await db.execute(sql, parameters=param, commit=True)
    if not file_ids:
        dt1 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        sql = "UPDATE periodic_task set date_finish=? where id=?"
        param = (dt1, periodic_task_row_id)
        await db.execute(sql, parameters=param, commit=True)


async def ss_checker(db, periodic_task_row_id=None, file_ids=[]):
    logger.info("получаем список ip для исключения из проверки")
    exclude_list_ip_url = os.getenv("EXCLUDE_LIST_IP")
    exclede_ip_check = await get_exclude_list(exclude_list_ip_url)
    logger.debug(", ".join(exclede_ip_check))
    su_pass = os.getenv('SUPER_ROOT_PASSWORD')

    if not periodic_task_row_id:
        sql= "INSERT INTO periodic_task (name, date_start) values(?,?);"
        '''YYYY-MM-DD HH:MM:SS'''
        dt1 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        param = ("CHK_SS", dt1)
        periodic_task_row_id = await db.execute(sql, parameters=param, commit=True)

    vpn_ckr = VPN_Checker(password=su_pass, db=db, logger=logger)
    if file_ids:
        sql = """
             select id, full_path, type_files from config_files cf 
             where id in ({0})
        """.format(",".join([str(id) for id in file_ids]))
        sql_del = "DELETE FROM validated where link_periodic_task_id = {0} and link_config_files_id in ({1});"
        sql_del = sql_del.format(periodic_task_row_id, ",".join([str(id) for id in file_ids]))
        res = await db.execute(sql_del, commit=True)
        dd = 0
    else:
        sql = "select id, full_path, type_files from config_files cf where delete_flg =false and type_files='SS'"
    ovpn_files = await db.execute(sql, fetchall=True)
    for file in ovpn_files:
        if check_ip_in_exclude_list(file[1], exclede_ip_check):
            logger.info(f"пропускаю файл {file[1]} потому что его IP в списке исключений")
            result = {"result": True, "reasone": "в спсике исключений"}
        else:
            result = await vpn_ckr.check_ss(file[1])

        sql = "INSERT INTO validated (link_config_files_id, link_periodic_task_id, date_exec, result_validate, reasone_error) VALUES(?,?,?,?,?)"
        dt2 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        param = (file[0], periodic_task_row_id, dt2, result["result"], result["reasone"])
        validated_row_id = await db.execute(sql, parameters=param, commit=True)
    if not file_ids:
        dt1 = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        sql = "UPDATE periodic_task set date_finish=? where id=?"
        param = (dt1, periodic_task_row_id)
        await db.execute(sql, parameters=param, commit=True)

async def get_exclude_list(url=None):
    result = list()
    try:
        data = requests.get(url, timeout=20)
        if data.status_code == 200:
            for row in data.text.split("\n"):
                result.append(row)
                logger.info(row)
        else:
            logger.info(f"статус ответа сервера {data.status_code}")
            raise Exception("Wrong response")
    except Exception as ex:
        logger.exception(f"при получении списков {url} на исключение возникла ошибка")
    return result

def check_ip_in_exclude_list(file_path, exclude_list=list(),):
    result = False
    ip_regexp = re.compile(r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}", flags=re.IGNORECASE|re.MULTILINE)
    ph = Path(file_path)
    if ph.is_dir():
        file_path = ph.joinpath("server-conf.txt")
    with open(file_path, 'r') as file_ss:
        in_file_text = file_ss.read(300)
    heve_ip_set = set()
    for ip in ip_regexp.findall(in_file_text):
        heve_ip_set.add(ip)

    for heve_ip in heve_ip_set:
        if heve_ip in exclude_list:
            result=True
            break
    return result



async def main(db, type=None, ovpn_files_root_folder=None, ss_file_root_folder=None):
    '''
    функция проверяет наличие файлов *.ovpn и если такие есть то добавляет их в список для чекинга
    :param ovpn_files_root_folder: папка с субпапками где хранятся ovpn файлы
    :return:
    '''

    if not ovpn_files_root_folder and not ss_file_root_folder:
        raise  Exception("Please fill 'ovpn_files_root_folder' or 'ss_file_root_folder' pareter")

    if type == 'ovpn':
        file_list = await get_all_ovpn_files_list(Path(ovpn_files_root_folder))
    elif type == 'ss':
        file_list = await get_all_ss_files_list(Path(ss_file_root_folder))
    else:
        raise Exception("Please set 'type' for file detect")

    sql = '''UPDATE config_files SET delete_flg=1 WHERE id>0'''
    if type == "ovpn":
        sql += " AND (type_files = 'OVPN' OR type_files='CONFIG');"
    else:
        sql += " AND type_files='SS';"
    await db.execute(sql, commit=True)
    for file in file_list:
        sql = "SELECT id FROM config_files WHERE full_path=(?);"
        res = await db.execute(sql, parameters=(file,), fetchone=True)
        if not res:
            if type == "ovpn":
                sql = "INSERT INTO config_files (full_path, delete_flg, date_add, type_files)"
                sql += " VALUES (?,?,?,?);"
                if Path(file).is_file():
                    res = await db.execute(sql, parameters=(file,
                                                            0,
                                                            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                                            'OVPN'),
                                           commit=True
                                           )
                else:
                    res = await db.execute(sql, parameters=(file,
                                                            0,
                                                            datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                                            'CONFIG'),
                                           commit=True
                                           )
            else:
                sql = "INSERT INTO config_files (full_path, delete_flg, date_add, type_files)"
                sql += " VALUES (?,?,?,?);"
                res = await db.execute(sql, parameters=(file,
                                                        0,
                                                        datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                                        'SS'),
                                       commit=True
                                       )
        else:
            sql = "UPDATE config_files SET delete_flg=0 WHERE full_path=(?);"
            await db.execute(sql, parameters=(file,), commit=True)


async def clear_db_old_rec(db, limit=1):
    logger.info("стартуем чистку БД")
    types_data = ['CHK_SS', 'CHK_VPN']
    ids = list()
    delete_ids = list()
    for tps in types_data:
        sql = "SELECT id FROM periodic_task WHERE name=(?) ORDER BY date_start desc limit (?);"
        res = await db.execute(sql, parameters=(tps,limit,), fetchall=True)
        for row in res:
            ids.append(row[0])
    ids_str = [str(id) for id in ids]
    logger.info("оставляем в БД айдишники {0}".format(", ".join(ids_str)))
    sql = "SELECT id FROM periodic_task;"
    res = await db.execute(sql, fetchall=True)
    for row in res:
        if row[0] not in ids:
            delete_ids.append(row[0])
    logger.info("необходимо удалить {0} задач".format(len(delete_ids)))
    for row_id in delete_ids:
        try:
            sql = "delete from validated where link_periodic_task_id = (?);"
            await db.execute(sql, parameters=(row_id,), commit=True)
            sql = "delete from periodic_task where id = (?);"
            await db.execute(sql, parameters=(row_id,), commit=True)
            logger.info(f"    успешно удалено {row_id}")
        except Exception as ex:
            logger.exception(f"ошибка удаления {row_id}")

if __name__ == "__main__":
    db = DataBase()
    # проверка чистки базы
    asyncio.run(clear_db_old_rec(db))
    # сборка файлов
    #path = os.getenv("SS_FILES_ROOT_FOLDER")
    #asyncio.run(main(db=db, type='ss', ss_file_root_folder=path))
    # проверка файлов
    #asyncio.run(ss_checker(db=db))
    # получаем списки на исключение
    #asyncio.run(get_exclude_list("https://api.tap2free.net/no-check"))
    #получаем список IP из конфиг файла
    #check_ip_in_exclude_list("/home/vintello/PycharmProjects/openvpn_checker/keys/ovpn/95.164.4.47/server-conf.txt", ['95.164.4.47', '95.164.4.42'])
    dd = 0
