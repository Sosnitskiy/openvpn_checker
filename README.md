проект по тестированю соединенений с openvpn серверами

# настройка сервера

необходимо чтобы на сервере были установлены OpenVPN и 

для установки OpenVpn выполните команду от имени администратора :

`apt install easy-rsa openvpn`

установка локального клиента для shadowsocks

   sudo apt install shadowsocks-libev




# установка бота

клонируем репозитарий на сервер

   `git clone git@gitlab.com:Sosnitskiy/openvpn_checker.git`


создаем файл ".env" в корне проекта

    sudo nano .env

вставляем строки и прописываем валидные данные

      SUPER_ROOT_PASSWORD=<Superadmin paswword>
      BOT_API_KEY=6860740060:.... 
      OVPN_FILES_ROOT_FOLDER=<полный путь к папке где хранятся ключи openvpn>
      SS_FILES_ROOT_FOLDER==<полный путь к папке где хранятся ключи ss>
      EXCLUDE_LIST_IP=https://api.tap2free.net/apis/servers/no-check
      IP_CHECK_URL=https://api.tap2free.net/chkip.php
      IP_CHECK_URL_TIMEOUT=10 <таймаут на проверку интренет соединияЮ
      ESTAB_CONN_TIMEOUT=30 <таймаут на установку соединения>
      SS_AWAIT_TIME=0.2 <задержкапроверки интренет соединия после установки соединенения>
      LOG_LEVEL=INFO

скрипт принимает два вида хранения ключей

1. непосредственно *.ovpn файлы
2. папка со структурой:
   * ca.crt
   * client1.crt
   * client1.key
   * server-conf.txt
    
   все имена файлов жестко зашиты в коде (!)
3. файлы с конфигурацией shadowsocks

создаем виртуальное окружение

    python3 -m venv env

активируем виртуальное окружение и дальше работаем только с ним 

      source env/bin/activate

Далее, необходимо установить все зависимости 

      pip install -r requirements.txt

# Добавляем автозапуск

Для этого необходимо единоразово выполнить следующую последовательность действий 
      
      cd ~
      sudo cp www/photocamrp/ovp_checker.service /lib/systemd/system/ovp_checker.service
      sudo systemctl enable ovp_checker
      sudo systemctl start ovp_checker


проверяем что сервис успешно запущен 

      sudo systemctl status ovp_checker


остановить-перезапустить сервис

      sudo systemctl stop ovp_checker
      sudo systemctl restart ovp_checker






