import asyncio
import datetime
import os
import functools
from database import DataBase
from aiogram import Bot, Dispatcher, Router, types
from aiogram.filters import CommandStart, Command
from aiogram import F
from aiogram.types import Message, BotCommand,InlineKeyboardMarkup, InlineKeyboardButton
import keybords
from aiogram.filters.callback_data import CallbackData
import utilites
import aiogram.utils.formatting
from aiogram.utils.formatting import (
    Bold, Text, Underline, TextLink, Code, as_list, as_marked_section, as_key_value, HashTag
)
from enum import Enum
from periodic_tasks import periodic1_timer, periodic2_timer, vpn_checker, ss_checker

from dotenv import load_dotenv
load_dotenv()

from config import settings


API_TOKEN = os.getenv('BOT_API_KEY')
dp = Dispatcher()
router = Router()
db = DataBase()
bot = Bot(API_TOKEN)#, parse_mode=ParseMode.HTML)

class Action_ovpn(str, Enum):
    # команды по open vpn
    last_scan = "last_scan_ovpn"
    statistic = "statistic_by_last_scan_ovpn"
    run_task = "run_task_ovpn"

class MyCallback(CallbackData, prefix="my"):
    # калбеки по open vpn
    action: Action_ovpn

# Команды для пользователей
user_commands = [
    BotCommand(command="start", description="текущая статистика"),
    BotCommand(command="open_vpn", description="работа с open_vpn конфигами"),
    BotCommand(command="shadowsocks", description="конфиги Shadowsocks")
]

@dp.message(CommandStart())
async def command_start_handler(message: Message, *args, **kwargs) -> None:
    kb = keybords.kb_main
    keyboard = types.ReplyKeyboardMarkup(
        keyboard=kb,
        resize_keyboard=True,
        input_field_placeholder="Сделайте выбор из меню"
    )
    await message.answer("Доброго дня", reply_markup=keyboard)

@dp.message(Command('open_vpn'))
async def work_with_ovpn(message: Message, *args, **kwargs):
    kb = keybords.kb_ovpn
    keyboard = types.ReplyKeyboardMarkup(
        keyboard=kb,
        resize_keyboard=True,
        input_field_placeholder="Сделайте выбор из меню"
    )
    await message.answer('Сейчас будем работать с только с конфигами Open VPN', reply_markup=keyboard)

@dp.message(Command('shadowsocks'))
async def work_with_ovpn(message: Message, *args, **kwargs):
    kb = keybords.kb_ss
    keyboard = types.ReplyKeyboardMarkup(
        keyboard=kb,
        resize_keyboard=True,
        input_field_placeholder="Сделайте выбор из меню"
    )
    await message.answer('Сейчас будем работать с только с конфигами Shadowsocks', reply_markup=keyboard)

 # OPEN VPN
@dp.message(F.text.lower() == "последний скан ovpn")
async def last_scan_ovpn(message: types.Message, *args, **kwargs):
    sql = """
    select pt.date_start, pt.date_finish from periodic_task pt 
    where name='CHK_VPN' 
    order by id desc
    LIMIT 1;
    """
    result = await db.execute(sql, fetchone=True)
    if result:
        mess = f"""
        останнє сканування проводилося {result[0]}
        """
        if result[1]:
            dt1 = datetime.datetime.strptime(result[0],"%Y-%m-%d %H:%M:%S" )
            dt2 = datetime.datetime.strptime(result[1],"%Y-%m-%d %H:%M:%S" )
            diff = dt2-dt1
            mess += f"\nСканування завершено {result[1]}. Виконувалось {diff.total_seconds()} сек."
        else:
            dt1 = datetime.datetime.strptime(result[0], "%Y-%m-%d %H:%M:%S")
            dt2 = datetime.datetime.now()
            diff = dt2-dt1
            mess += f"\nСканування ще не завершено. виконується вже впродовж {diff.total_seconds()} сек."
    else:
        mess = "сканування ще не проводилося жодного разу"
    await message.reply(mess)

@dp.message(F.text.lower() == "cтатистика проверки по ovpn")
async def statistic_ovpn(message: types.Message):
    mess = ""
    sql = """
        select pt.id, pt.date_start, pt.date_finish from periodic_task pt 
        where name='CHK_VPN' 
        order by id desc
        LIMIT 1;
        """
    result = await db.execute(sql, fetchone=True)
    if not result:
        mess = "сканування ще не проводилося жодного разу"
    else:
        last_task_id = result[0]
        mess+= f"последняя проверка производилась c {result[1]} по {result[2]}"
        sql = '''
        select cf_f.full_path, vl.date_exec, vl.reasone_error, cf_f.id from validated vl
    LEFT OUTER JOIN config_files cf_f on(vl.link_config_files_id = cf_f.id)
    where vl.link_periodic_task_id=?
    and vl.result_validate=?;
        '''
        param = (last_task_id, 1)
        result1 = await db.execute(sql, parameters=param, fetchall=True)
        len_result1 = len(result1)
        param = (last_task_id, 0)
        result0 = await db.execute(sql, parameters=param, fetchall=True)
        len_result0 = len(result0)
        sum_len = len_result1+len_result0
        mess_ = list()
        mess_1= list()
        mess_2 = list()
        mess_1.append(Text(f"Всего было проверено {sum_len} служб"))
        mess_2.append(Text(f"успешно прошли проверку {len_result1} служб"))
        mess_2.append(Text(f"не прошли проверку {len_result0} служб:"))
        mess_as_list= list()
        for row in result0:
            ip_from_folder_file_name = utilites._get_ip_from_file_path(row[0])
            int_by_delta_time, int_by_one_time = await utilites._get_statistic_by_file(db, row[3], 30)
            mess_as_list.append(aiogram.utils.formatting.as_list(*[Text("- "),
                                                                   Text(ip_from_folder_file_name[0]),
                                                                   Code(f"{ip_from_folder_file_name[1]}"),
                                                                   Text(ip_from_folder_file_name[2]),
                                                                   Text(f"   | {row[1]} "),
                                                                   Text(f"   {int_by_one_time} ({int_by_delta_time})"),
                                                                   Text(f"\n{row[2]}")
                                                                   ],
                                                                 sep=""),

                                )
        if mess_as_list:
            mess_2.append(aiogram.utils.formatting.as_list(*mess_as_list, sep="\n"))
        mess_1.append(aiogram.utils.formatting.as_list(*mess_2,))
        mess_.append(aiogram.utils.formatting.as_list(*mess_1, sep="\n"))
    content = as_list(*mess_, sep="\n\n", )

    text_reply = content.as_kwargs()
    text_limit = 4096
    if len(text_reply['text']) > text_limit:
        msgs_chunks = [text_reply['text'][i:i + text_limit] for i in range(0, len(text_reply['text']), text_limit)]
        for text in msgs_chunks:
            await message.reply(text=text)
    else:
        await message.reply(**text_reply, reply_markup=keybords.keyboard_repeat_check_error_ovpn)

@dp.message(F.text.lower() == "выполнить проверку ovpn")
async def run_scan_ovpn(message: types.Message):
    chk = await _check_if_locked(db)
    if chk["lock"]:
        mess = "Отказано. Повторите позже. {0}".format(chk["reasone"])
    else:
        asyncio.create_task(vpn_checker(db))
        mess = "Задача запущена"
    await message.reply(mess)



# SHADOWSOCKS
@dp.message(F.text.lower() == "последний скан ss")
async def last_scan_ss(message: types.Message, *args, **kwargs):
    sql = """
    select pt.date_start, pt.date_finish from periodic_task pt 
    where name='CHK_SS' 
    order by id desc
    LIMIT 1;
    """
    result = await db.execute(sql, fetchone=True)
    if result:
        mess = f"""
        останнє сканування проводилося {result[0]}
        """
        if result[1]:
            dt1 = datetime.datetime.strptime(result[0],"%Y-%m-%d %H:%M:%S" )
            dt2 = datetime.datetime.strptime(result[1],"%Y-%m-%d %H:%M:%S" )
            diff = dt2-dt1
            mess += f"\nСканування завершено {result[1]}. Виконувалось {diff.total_seconds()} сек."
        else:
            dt1 = datetime.datetime.strptime(result[0], "%Y-%m-%d %H:%M:%S")
            dt2 = datetime.datetime.now()
            diff = dt2-dt1
            mess += f"\nСканування ще не завершено. виконується вже впродовж {diff.total_seconds()} сек."
    else:
        mess = "сканування ще не проводилося жодного разу"
    await message.reply(mess)

@dp.message(F.text.lower() == "cтатистика проверки по ss")
async def statistic_ss(message: types.Message):
    mess = ""
    sql = """
        select pt.id, pt.date_start, pt.date_finish from periodic_task pt 
        where name='CHK_SS' 
        order by id desc
        LIMIT 1;
        """
    result = await db.execute(sql, fetchone=True)
    if not result:
        mess = "сканування ще не проводилося жодного разу"
    else:
        last_task_id = result[0]
        mess+= f"последняя проверка производилась c {result[1]} по {result[2]}"
        sql = '''
            select cf_f.full_path, vl.date_exec, vl.reasone_error, cf_f.id from validated vl
            LEFT OUTER JOIN config_files cf_f on(vl.link_config_files_id = cf_f.id)
            where vl.link_periodic_task_id=?
            and vl.result_validate=?;
        '''
        param = (last_task_id, 1)
        result1 = await db.execute(sql, parameters=param, fetchall=True)
        len_result1 = len(result1)
        param = (last_task_id, 0)
        result0 = await db.execute(sql, parameters=param, fetchall=True)
        len_result0 = len(result0)
        sum_len = len_result1+len_result0
        mess_ = list()
        mess_1 = list()
        mess_2 = list()
        mess_1.append(Text(f"Всего было проверено {sum_len} служб"))
        mess_2.append(Text(f"успешно прошли проверку {len_result1} служб"))
        mess_2.append(Text(f"не прошли проверку {len_result0} служб:"))
        mess_as_list = list()
        for row in result0:
            ip_from_folder_file_name = utilites._get_ip_from_file_path(row[0])
            int_by_delta_time, int_by_one_time = await utilites._get_statistic_by_file(db, row[3], 30)
            mess_as_list.append(aiogram.utils.formatting.as_list(*[Text("- "),
                                                                   Text(ip_from_folder_file_name[0]),
                                                                   Code(f"{ip_from_folder_file_name[1]}"),
                                                                   Text(ip_from_folder_file_name[2]),
                                                                   Text(f"   | {row[1]} "),
                                                                   Text(f"   {int_by_one_time} ({int_by_delta_time})"),
                                                                   Text(f"\n{row[2]}")
                                                                   ],
                                                                 sep=""),

                                )
        if mess_as_list:
            mess_2.append(aiogram.utils.formatting.as_list(*mess_as_list, sep="\n"))
        mess_1.append(aiogram.utils.formatting.as_list(*mess_2, ))
        mess_.append(aiogram.utils.formatting.as_list(*mess_1, sep="\n"))

    content = as_list(*mess_, sep="\n\n", )
    text_reply = content.as_kwargs()
    text_limit = 4096
    if len(text_reply['text']) > text_limit:
        msgs_chunks = [text_reply['text'][i:i + text_limit] for i in range(0, len(text_reply['text']), text_limit)]
        for text in msgs_chunks:
            await message.reply(text=text)
    else:

        await message.reply(**text_reply, reply_markup=keybords.keyboard_repeat_check_error_ss)


@dp.callback_query(F.data =="button_check_error_ss")
async def second_checker_errors_ss(callback_query: types.CallbackQuery, *args, **kwargs):
    last_periodic_task_id = await utilites.get_last_periodic_task_id(db, "CHK_SS")
    sql = """
     select v.link_config_files_id  from validated v 
     where v.link_periodic_task_id = {0}
     and v.result_validate = FALSE
    """.format(last_periodic_task_id)
    result1 = await db.execute(sql, fetchall=True)
    ids = [row[0] for row in result1]
    chk_task = asyncio.create_task(ss_checker(db, last_periodic_task_id, ids))
    chk_task.add_done_callback(functools.partial(informer, callback_query.from_user.id, "Повторная проверка сбойных завершена"))
    mess = "Задача запущена"
    await bot.send_message(callback_query.from_user.id, mess)


@dp.callback_query(F.data =="button_check_error_ovpn")
async def second_checker_errors_ovpn(callback_query: types.CallbackQuery, *args, **kwargs):
    last_periodic_task_id = await utilites.get_last_periodic_task_id(db, "CHK_VPN")
    sql = """
     select v.link_config_files_id  from validated v 
     where v.link_periodic_task_id = {0}
     and v.result_validate = FALSE
    """.format(last_periodic_task_id)
    result1 = await db.execute(sql, fetchall=True)
    ids = [row[0] for row in result1]
    chk_task = asyncio.create_task(vpn_checker(db, last_periodic_task_id, ids))
    chk_task.add_done_callback(functools.partial(informer, callback_query.from_user.id, "Повторная проверка сбойных завершена"))
    mess = "Задача запущена"
    await bot.send_message(callback_query.from_user.id, mess)

def informer(user_id, mess, *args, **kwargs):
    asyncio.create_task(bot.send_message(user_id, mess))

@dp.message(F.text.lower() == "выполнить проверку ss")
async def run_scan_ss(message: types.Message):
    asyncio.create_task(ss_checker(db))
    mess = "Задача запущена"
    await message.reply(mess)

@dp.message(F.text.lower() == "последние сканы")
async def total_last_scans(message: types.Message, *args, **kwargs):
    sql = """
        select pt.id, pt.date_start, pt.date_finish from periodic_task pt 
        where name=? 
        order by id desc
        LIMIT 1;
        """
    content_list = list()
    for num, row in enumerate(('CHK_VPN', 'CHK_SS')):
        result = await db.execute(sql, parameters=((row),), fetchone=True)
        mess = list()
        if num == 0:
            mess.append(Bold("Open VPN:"))
        else:
            mess.append(Bold("Shadowsocks:"))
        if not result:
            mess.append(as_key_value("сканирование еще не проводилось ниразу", ""))
        else:
            mess.append(as_key_value("Stearted", result[1]))
            mess.append(as_key_value("Finished", result[2]))
            strt_dt = datetime.datetime.strptime(result[1], "%Y-%m-%d %H:%M:%S")
            end_dt = datetime.datetime.strptime(result[2], "%Y-%m-%d %H:%M:%S") if result[2] else 0
            if end_dt:
                leng = end_dt - strt_dt
                leng_s = f"{leng}"
            else:
                leng_s = "in progress"
            mess.append(as_key_value("Duration", leng_s))

        content_list.append(as_marked_section(*mess, marker="    ", ))

    content = as_list(*content_list, sep="\n\n", )
    await message.reply(**content.as_kwargs())

@dp.message(F.text.lower() == "cтатистика")
async def total_statistics(message: types.Message, *args, **kwargs):
    sql = """
            select pt.id, pt.date_start, pt.date_finish from periodic_task pt 
            where name=?
            order by id desc
            LIMIT 1;
            """
    result = await db.execute(sql, parameters=["CHK_VPN"], fetchone=True)
    last_ovpn_check_id = result[0]

    result = await db.execute(sql, parameters=["CHK_SS"], fetchone=True)
    last_ss_check_id = result[0]
    sql = """
        select result_validate, count(*) from validated
        where link_periodic_task_id = ?
        group by result_validate
        """
    result = dict()
    content_list = list()
    for num, row_id in enumerate((last_ovpn_check_id, last_ss_check_id)):
        result = await db.execute(sql, parameters=[row_id], fetchall=True)
        mess = list()
        if num == 0:
            mess.append(Bold("Open VPN:"))
        else:
            mess.append(Bold("Shadowsocks:"))
        summ = 0
        for row in result:
            summ += row[1]
            if row[0] == "0":
                mess.append(as_key_value("Failed", row[1]))
            elif row[0] == '1':
                mess.append(as_key_value("Success", row[1]))

        mess.append(as_key_value("Total", summ))
        content_list.append(as_marked_section(*mess,marker="    ",))

    content = as_list(*content_list, sep="\n\n",)
    await message.reply(**content.as_kwargs())


@dp.message(F.text.lower() == "доступность")
async def avaibles_servers(message: types.Message, *args, **kwargs):
    gorizont_day = 30
    divider_message = 50
    mess_as_list = list()
    messages = list()
    result = []
    sql_cf_files = """
        select cf.id, cf.full_path  from config_files cf 
        where cf.delete_flg =False;
    """
    result_cf = await db.execute(sql_cf_files, fetchall=True)
    sql_res_val = """
        select count(*) from validated v 
        where v.link_config_files_id ={0}
        and v.date_exec > datetime('now', "-{1} day")
        and result_validate =0;
    """
    for row in result_cf:
        ip_file_folder = utilites._get_ip_from_file_path(row[1])
        if not ip_file_folder:
            from_file = utilites._get_ips_from_file(row[1])
            if from_file:
                ip_file_folder = []
                ip_file_folder.append("")
                ip_file_folder.append(from_file[0])
                ip_file_folder.append("")
            else:
                ip_file_folder.append("")
                ip_file_folder.append(row[1])
                ip_file_folder.append("")
        errors_markers = await utilites._get_statistic_by_file(db, row[0], gorizont_day)
        result.append({"id": row[0],
                       "full_path": ip_file_folder if ip_file_folder else utilites._get_ips_from_file(row[1]),
                       "goriz": errors_markers[0] if errors_markers else 0,
                       "errors_24h": errors_markers[1] if errors_markers else 0
                       })
    max_len = 0
    sorted_list = utilites.sort_ips(result)
    for el in sorted_list:
        sum_len = 0
        for el_sub in el["full_path"]:
            sum_len += len(el_sub)
        if max_len < sum_len:
            max_len = sum_len
    mess_as_list.append(f"          IP           Ошибок: 24часа | За {gorizont_day} дней")
    for num, res in enumerate(sorted_list, start=1):

        text_list = []
        len_data = 0
        if res['full_path']:
            if res['full_path'][0]:
                text_list.append(Text(res['full_path'][0]))
                len_data += len(res['full_path'][0])
            if res['full_path'][1]:
                text_list.append(Code(f"{res['full_path'][1]}"))
                len_data += len(res['full_path'][1])
            if res['full_path'][2]:
                text_list.append(Text(res['full_path'][2]))
                len_data += len(res['full_path'][2])
            blank_chars = max_len - len_data
            if blank_chars > 0:
                text_list.append(Text(" ", "\t"*blank_chars, ))
        else:
            dd = 0
        text_list.append(Text(f"     {res['errors_24h']} | {res['goriz']}"))

        mess_as_list.append(aiogram.utils.formatting.as_list(*text_list,sep=""))

        if num % divider_message == 0:
            messages.append(aiogram.utils.formatting.as_list(*mess_as_list, sep="\n"))
            mess_as_list = list()
    if mess_as_list:
        messages.append(aiogram.utils.formatting.as_list(*mess_as_list, sep="\n"))
    for repl in messages:
        await message.reply(**repl.as_kwargs())
@dp.message(F.text.regexp(r'(?:[0-9]{1,3}\.){3}[0-9]{1,3}'))
async def get_server_statistic(message: types.Message, *args, **kwargs):
    ip = message.text
    sql = f'''
    select id, full_path from config_files cf 
    where cf.full_path like '%{ip}%'
    and delete_flg = FALSE;
    '''
    conf_files = await db.execute(sql, fetchall=True)
    for row_conf_file in conf_files:
        sql = '''
        select v.date_exec, v.result_validate, v.reasone_error  from validated v 
        where v.link_config_files_id =(?)
        order BY v.date_exec DESC 
        '''

        result = await db.execute(sql, parameters=[row_conf_file[0],],fetchall=True)
        messages = list()
        message_ = list()
        divider = 30
        ip_file_folder = utilites._get_ip_from_file_path(row_conf_file[1])
        message_.append(Bold("".join(ip_file_folder)))
        if result:
            for num, row in enumerate(result, start=1):
                if row[1] == '1':
                    message_.append(aiogram.utils.formatting.as_list(*[Text(row[0]),
                                                                       Text(" | "),
                                                                       Text(" OK"),
                                                                       Text("  {0}".format(row[2]) if row[2] else "")
                                                                       ],
                                                                     sep=""))
                else:
                    message_.append(aiogram.utils.formatting.as_list(*[Bold(row[0]),
                                                                      Bold(" | "),
                                                                      Bold(row[2])
                                                                      ],
                                                                    sep=""))
                if num % divider == 0:
                    messages.append(aiogram.utils.formatting.as_list(*message_, sep="\n "))
                    message_ = list()
            if message_:
                messages.append(aiogram.utils.formatting.as_list(*message_, sep="\n "))
        else:
            message_.append(Text("    нет данных"))
            messages.append(aiogram.utils.formatting.as_list(*message_, sep="\n "))
            pass
        for repl in messages:
            await message.reply(**repl.as_kwargs())
    if not conf_files:
        reply_ = f"{ip} не найден"
        await message.reply(reply_)
async def main_bot():
    try:
        # инициируем работу с БД - создаем таблицы по умолчанию, если это необходимо
        await asyncio.create_task(db.create_tables_by_default())
        # задача , которая периодически сканирует папку на предмет наличия новых конфигов
        periodic1_task = asyncio.create_task(periodic1_timer(db=db))
        # задача запускает по времени проверку конфигов
        periodic2_task = asyncio.create_task(periodic2_timer(db=db))

        await bot.set_my_commands(user_commands)
        # And the run events dispatching
        await dp.start_polling(bot)
    except Exception as ex:
        dd = 0
    finally:
        periodic1_task.cancel()
        periodic2_task.cancel()
        try:
            await periodic1_task
            await periodic2_task
        except asyncio.CancelledError:
            print("periodic tasks canceled")

async def _check_if_locked(db):
    res = {'lock':False, 'reasone':None}
    sql = '''
    select pt.id, pt.date_start, pt.date_finish, pt.name from periodic_task pt 
    order by pt.date_start DESC
    LIMIT 1;
    '''
    result = await db.execute(sql, fetchone=True)
    if result and result[2] == None:
        res['lock'] = True
        point = "Shadowsocks" if result[3] == "CHK_SS" else "OpenVPN"
        res['reasone'] = f"Запущен процесс по сканированию {point}"
    return res

if __name__ == "__main__":
    asyncio.run(main_bot())
