1. выполнить остановку службы 

    `sudo systemctl stop ovp_checker`

2. переименовать старую папку с проектом в *_old
    
    mv <old_name> <old_name>_old
3. 
2. выполнить клонирование репозитария

    `git clone git@gitlab.com:Sosnitskiy/openvpn_checker.git`

3. скопировать из папки <old_name>_old файл .env
4. 
4. добавить в файл .env строку ?где нужно указать путь к коренной папке где хранятся точки подключения SS
    SS_FILES_ROOT_FOLDER=/home/vintello/PycharmProjects/openvpn_checker/keys/ss
5. создаем виртуальное окружение

    python3 -m venv env

активируем виртуальное окружение и дальше работаем только с ним 

      source env/bin/activate

Далее, необходимо установить все зависимости 

      pip install -r requirements.txt
6. запускаем сервис
    
    `sudo systemctl start ovp_checker`