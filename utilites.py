import os
import re
import base64
from pathlib import Path
from database import DataBase
import struct
import socket
import asyncio
import ipaddress


def _get_ip_from_file_path(path_file_or_folder):
    """
    /home/vintello/PycharmProjects/openvpn_checker/keys/ovpn/95.164.11.17
    /home/vintello/PycharmProjects/openvpn_checker/keys/ss/168.119.57.171.ss
    на выходе:
        ('', '95.164.11.17', '')
        ('', '168.119.57.171', '.ss')
    """
    ip_regexp = re.compile(r'^(?P<prefix>.*?)(?P<ip>\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?P<suffix>.*?)$')
    path_file_or_folder = Path(path_file_or_folder)
    result = None
    file_folder = path_file_or_folder.name
    split_data = ip_regexp.match(file_folder)
    if split_data:
        result = split_data.groups()
    return result


def _get_ips_from_file(path_file_or_folder):
    if os.path.isdir(path_file_or_folder):
        file = os.path.join(path_file_or_folder, "server-conf.txt")
    else:
        file = path_file_or_folder
    ip_regexp = re.compile(r"(?:[0-9]{1,3}\.){3}[0-9]{1,3}", flags=re.IGNORECASE | re.MULTILINE)
    with open(file, 'r') as file_ss:
        in_file_text = file_ss.read(300)
    heve_ip_set = set()
    for ip in ip_regexp.findall(in_file_text):
        heve_ip_set.add(ip)
    heve_ip_list = [ip for ip in heve_ip_set]
    return heve_ip_list


def _decode_base64(data, altchars=b'+/'):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    data = re.sub(rb'[^a-zA-Z0-9%s]+' % altchars, b'', data)  # normalize
    missing_padding = len(data) % 4
    if missing_padding:
        data += b'='* (4 - missing_padding)
    return base64.b64decode(data, altchars)


async def _get_statistic_by_file(db, file_id, delta=30):
    int_by_delta_time = 0
    int_by_one_time = 0
    sql = """select count(*) as cnt from validated v 
            where v.link_config_files_id = {1}
            and v.date_exec > datetime('now', '-{0} day')
            and v.result_validate = FALSE;
        """
    sql1 = sql.format(delta, file_id)
    result = await db.execute(sql1,  fetchone=True)
    if result:
        int_by_delta_time = result[0]
    sql2 = sql.format("1", file_id)
    result2 = await db.execute(sql2, fetchone=True)
    if result2:
        int_by_one_time = result2[0]
    return (int_by_delta_time, int_by_one_time)


def sort_ips(ips_):
    sortedkey = sorted(ips_, key = lambda x: ipaddress.IPv4Address(x["full_path"][1]))
    return sortedkey


async def get_last_periodic_task_id(db, type_task):
    result_row_id = None
    sql = '''
    select pt.id from periodic_task pt 
    where pt.name='{0}' 
    order by date_start desc
    limit 1;
    '''
    sql_ex = sql.format(type_task)
    result = await db.execute(sql_ex, fetchone=True)
    if result:
        result_row_id = result[0]
    return result_row_id

if __name__ == "__main__":
    #_get_ip_from_file_path("/home/vintello/PycharmProjects/openvpn_checker/keys/ovpn/95.164.11.17")
    #_get_ip_from_file_path("/home/vintello/PycharmProjects/openvpn_checker/keys/ss/168.119.57.171.ss")
    db = DataBase("main.db")
    # проверка чистки базы
    ff = asyncio.run(get_last_periodic_task_id(db, "CHK_SS"))
    print(ff)
    dd =0