import os

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    DB_DATABASE: str = os.environ.get("DB_DATABASE", "delivery-service")
    DB_USERNAME: str = os.environ.get("DB_USERNAME", "postgres")
    DB_PASSWORD: str = os.environ.get("DB_PASSWORD", "postgres")
    DB_HOST: str = os.environ.get("DB_HOST", "localhost")
    DB_PORT: int = os.environ.get("DB_PORT", 5432)

    SQLALCHEMY_DATABASE_URL: str = f"postgresql+asyncpg://{DB_USERNAME}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_DATABASE}"


settings = Settings()