import asyncio
import aiohttp
from aiohttp_socks import ProxyType, ProxyConnector, ChainProxyConnector
import os.path
import requests
from utilites import _decode_base64
import re
from database import DataBase

from dotenv import load_dotenv
load_dotenv()

from log_set import init_logger

class VPN_Checker:
    def __init__(self, password, db, logger=None):
        self.log = None
        if logger:
            self.log = logger
        else:
            self.log = init_logger("info")
        self.loop = asyncio.get_event_loop()
        self.db = db
        self.password = password if password else None
        self.my_ip = self.get_current_ip()

    def get_current_ip(self):
        ip = None
        try:
            ip_check_url_str = self._get_os_env("IP_CHECK_URL", str, "https://api.ipify.org")
            ip_check_urls = [ip_check_url.strip() for ip_check_url in ip_check_url_str.split(";")]
            ip_check_url_timeout = self._get_os_env("IP_CHECK_URL_TIMEOUT", float, 10)
            for url in ip_check_urls:
                response = requests.get(url, timeout=ip_check_url_timeout)
                if response.status_code in (200, 201, 204):
                    ip = response.text
                    break
        except Exception as ex:
            self.log.exception("error in get_current_ip")
        return ip

    async def _make_requests(self, async_session: aiohttp.ClientSession, url: str, timeout=10):
        result = None
        try:
            async with async_session.get(url=url, timeout=timeout) as _response:
                result = await _response.text()
        except Exception as ex:
            self.log.exception("error in _make_requests")
        return result


    async def _get_ip_async(self, proxy=None):
        ip_check_url_str = self._get_os_env("IP_CHECK_URL", str, "https://api.ipify.org")
        ip_check_url_timeout = self._get_os_env("IP_CHECK_URL_TIMEOUT", float, 10)
        ip = None
        ip_check_urls = [ip_check_url.strip() for ip_check_url in ip_check_url_str.split(";")]
        try:
            tasks = []
            if proxy:
                connector = ProxyConnector.from_url('socks5://127.0.0.1:1081', rdns=True)
                async with aiohttp.ClientSession(connector=connector) as async_session:
                    for url in ip_check_urls:
                        task = asyncio.create_task(
                            coro=self._make_requests(
                                async_session=async_session,
                                url=url,
                                timeout=ip_check_url_timeout
                            )
                        )
                        tasks.append(task)
                    res = await asyncio.gather(*tasks, return_exceptions=False)
                    ip = set(res)
            else:
                async with aiohttp.ClientSession() as async_session:
                    for url in ip_check_urls:
                        task = asyncio.create_task(
                            coro=self._make_requests(
                                async_session=async_session,
                                url=url,
                                timeout=ip_check_url_timeout
                            )
                        )
                        tasks.append(task)
                    res = await asyncio.gather(*tasks, return_exceptions=False)
                    ip = set(res)
        except Exception as ex:
            self.log.exception("error in _get_ip")
        return ip

    async def _get_ip(self, url_checker, proxy=None):
        ip_check_url_timeout = self._get_os_env("IP_CHECK_URL_TIMEOUT", float, 10)
        ip = None
        try:
            if proxy:
                request = requests.get(url_checker,
                                  proxies=dict(http='socks5://127.0.0.1:1081',
                                               https='socks5://127.0.0.1:1081'),
                                  timeout=ip_check_url_timeout)

            else:
                request = requests.get(url_checker, timeout=ip_check_url_timeout)
            print(request.status_code)
            if request.status_code in (200, 201, 204):
                ip = request.text
        except Exception as ex:
            self.log.exception("error in _get_ip")
        if ip:
            ip = set((ip,))
        return ip

    async def check_openvpn(self, config_file=None, type_file=None):
        ip_check_url = self._get_os_env("IP_CHECK_URL", str, "https://api.ipify.org")
        ip_check_urls = [ip_check_url.strip() for ip_check_url in ip_check_url.split(";")]
        ip_check_url_timeout = self._get_os_env("IP_CHECK_URL_TIMEOUT", float, 10)
        ip_check_url_timeout = ip_check_url_timeout * len(ip_check_urls) + 2
        estab_conn_timeout = self._get_os_env("ESTAB_CONN_TIMEOUT", float, 30)
        result = None
        reasone = None
        try:
            self.log.info("пробуем открыть VPN соединение")
            try:
                result = await asyncio.wait_for(self.open_vpn_connection(config_file, type_file), timeout=estab_conn_timeout)
                if result == True:
                    self.log.info("Success opened")
                else:
                    reasone = "не удалось открыть VPN соединение"
            except asyncio.exceptions.TimeoutError:
                print('Gave up waiting, task "open_vpn_connection" canceled')
                self.log.info("closed by timeout")
                self.log.exception("module ")
                result = False
                reasone = "таймаут на установление VPN соединения"
            except asyncio.exceptions.CancelledError:
                self.log.exception(f'Task run failed with exception {ex}')
                result = False
            except Exception as ex:
                self.log.exception(f'Task run failed with exception {ex}')
                reasone = "другая ошибка при установлке VPN соединения. смотри лог файл"
                result = False
            if result:
                try:
                    self.log.info("check ip")
                    result = await asyncio.wait_for(self.check_connect(), timeout=ip_check_url_timeout)
                    if not result:
                        reasone = "через ВПН соединенеие нет трафика"
                        result = False
                except asyncio.TimeoutError:
                    self.log.info("task 'check_connect' closed by timeout")
                    print('Gave up waiting, task canceled')
                    reasone = "таймаут на проверку установленного IP адреса"
                    result = False
                except Exception as ex:
                    self.log.exception("ffffff111")
                    reasone = "другая ошибка при проверке установленного IP адреса"
                    result = False
            else:
                result = False

        except Exception as ex:
            self.log.exception("run")
            reasone = "другая ошибка при проверке. смотри лог"
            result = False
        finally:
            self.log.info("finally")
            await self.kill_vpn_connection(type_vpn="openvpn")
        return {"result": result, "reasone": reasone}

    def _get_os_env(self, name, transf_to_type, default):
        val = os.getenv(name)
        if val:
            val = transf_to_type(val.strip().replace(",","."))
        elif default:
            val = transf_to_type(default.strip().replace(",","."))
        return val

    async def check_ss(self, config_file=None):
        ss_await_time = self._get_os_env("SS_AWAIT_TIME", float, 1)
        ip_check_url = self._get_os_env("IP_CHECK_URL", str, "https://api.ipify.org")
        ip_check_urls = [ip_check_url.strip() for ip_check_url in ip_check_url.split(";")]
        ip_check_url_timeout = self._get_os_env("IP_CHECK_URL_TIMEOUT", float, 10)
        ip_check_url_timeout = ip_check_url_timeout * len(ip_check_urls) + 2
        estab_conn_timeout = self._get_os_env("ESTAB_CONN_TIMEOUT", float, 30)
        result = None
        reasone = None
        try:
            self.log.info("пробуем открыть SS соединение")
            try:
                result = await asyncio.wait_for(self.open_ss_connection(config_file,), timeout=estab_conn_timeout)
                if result == True:
                    self.log.info("Success opened")
                else:
                    reasone = "не удалось открыть SS соединение"
            except asyncio.exceptions.TimeoutError:
                print('Gave up waiting, task "open_vpn_connection" canceled')
                self.log.info("closed by timeout")
                self.log.exception("module ")
                result = False
                reasone = "таймаут на установление SS соединения"
            except asyncio.exceptions.CancelledError as ex:
                self.log.exception(f'Task run failed with exception {ex}')
                result = False
            except Exception as ex:
                self.log.exception(f'Task run failed with exception {ex}')
                reasone = "другая ошибка при установлке SS соединения. смотри лог файл"
                result = False
            if result:
                try:
                    await asyncio.sleep(ss_await_time)
                    self.log.info("check ip")
                    result = await asyncio.wait_for(self.check_connect(proxy=True), timeout=ip_check_url_timeout*2)
                    if not result:
                        reasone = "через SS соединенеие нет трафика"
                        result = False
                except asyncio.TimeoutError:
                    self.log.info("task 'check_connect' closed by timeout")
                    print('Gave up waiting, task canceled')
                    reasone = "таймаут на проверку установленного IP адреса"
                    result = False
                except Exception as ex:
                    self.log.exception("ffffff111")
                    reasone = "другая ошибка при проверке установленного IP адреса"
                    result = False
            else:
                result = False

        except Exception as ex:
            self.log.exception("run")
            reasone = "другая ошибка при проверке. смотри лог"
            result = False
        finally:
            self.log.info("finally")
            await self.kill_vpn_connection(type_vpn="ss")
        return {"result": result, "reasone": reasone}

    async def check_connect(self, proxy=False):
        self.log.info(f"current IP is : {self.my_ip}")
        result = False
        ip_check_url = self._get_os_env("IP_CHECK_URL", str, "https://api.ipify.org")
        ip_check_urls = [ip_check_url.strip() for ip_check_url in ip_check_url.split(";")]

        for url_for_check in ip_check_urls:
            try:
                ips = await self._get_ip(url_for_check, proxy=proxy)
                if ips:
                    self.log.info(f"after connect IP is : {ips} проверяли через {url_for_check}")
                    if self.my_ip not in ips and ips != {None}:
                        result = True
                    break
                else:
                    self.log.error("не удалось получить IP через соедиенение. использовался сервис {0}".format(url_for_check))
            except Exception as ex:
                self.log.exception("проверка наличия интренета через {url_for_check}")
        return result

    async def open_ss_connection(self, config_file):
        self.log.info("формируем команду для открытия shadowsocks соединения")
        self.log.info(f"    file - {config_file}")
        result = False
        struct_data = re.compile(r"ss:\/\/(.*?)@((?:[0-9]{1,3}\.){3}[0-9]{1,3}):(\d*?)#(.*?)$")
        struct_unhached_data = re.compile(r"^(.+?):(.+?)$")
        with open(config_file, 'r') as file_ss:
            in_file_text = file_ss.read(300)
        pars = struct_data.match(in_file_text)
        if not pars:
            raise Exception(f"по какой-то причине структура файла {config_file} является неправильной и не может быть обработана регуляркой")
        hashed_data, ip, port, serv_name = pars.groups()
        #prot_passw = base64.b64decode(hashed_data).decode("'utf-8'")
        prot_passw = _decode_base64(hashed_data.encode()).decode("'utf-8'")
        pars = struct_unhached_data.match(prot_passw)
        if not pars:
            raise Exception("нет возможности получить пароль и модель расшифровки")
        encr_model, passw = pars.groups()
        '''ss-local -s 81.28.6.186 -p 51348 -b 127.0.0.1 -l 1081 -k 7W2ZbLmlcZlnZaJwJOFOeGmZ3YfN4KWXLKgRJqVfy7iZVU7u -m chacha20-ietf-poly1305'''
        cmd = "ss-local"
        cmd += f" -s {ip}"
        cmd += f" -p {port}"
        cmd += " -b 127.0.0.1 -l 1081"
        cmd += f" -k {passw}"
        cmd += f" -m {encr_model}"
        self.log.info(cmd)
        process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE)
        row_byte = await process.stdout.readline()
        self.log.info("в потоке ответа пытаемся найти ключевые слова")
        while row_byte:
            # Do stuff with byte.
            row_str = row_byte.decode()
            self.log.debug(row_str)
            if "listening at 127.0.0.1:1081" in row_str:
                self.log.info("---- Sucess in ----- ")
                self.log.info(row_str)
                result = True
                break
            row_byte = await process.stdout.readline()
        process.terminate()
        return result

    async def open_vpn_connection(self, ovpn_file=None, type_file=None):
        self.log.info("формируем команду для открытия ВПН")
        result = False
        # create as a subprocess using create_subprocess_shell
        if ovpn_file and type_file == "OVPN":
            self.log.info(f"команда формируется из ovpn файла {ovpn_file}")
            cmd = f'echo {self.password}| sudo -S openvpn --config {ovpn_file}'
            self.log.info("сформирована команда:")
            self.log.info(cmd)
            process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE)
        elif ovpn_file and type_file == "CONFIG":
            self.log.info(f"команда формируется из директории {ovpn_file} с server-conf.txt файлом и ключами")
            cmd = f'echo {self.password}| sudo -S openvpn'
            cmd += ' --config {0}'.format(os.path.join(ovpn_file, "server-conf.txt"))
            cmd += ' --ca {0}'.format(os.path.join(ovpn_file, "ca.crt"))
            cmd += ' --cert {0}'.format(os.path.join(ovpn_file, "client1.crt"))
            cmd += ' --key {0}'.format(os.path.join(ovpn_file, "client1.key"))
            self.log.info("сформирована команда:")
            self.log.info(cmd)
            process = await asyncio.create_subprocess_shell(cmd, stdout=asyncio.subprocess.PIPE)
        else:
            self.log.error(f"совершенно не понятно как это '{ovpn_file}' попало в БД")
        # report the details of the subprocess
        row_byte = await process.stdout.readline()
        self.log.info("в потоке ответа пытаемся найти ключевые слова")
        while row_byte:
            # Do stuff with byte.
            row_str = row_byte.decode()
            if "Initialization Sequence Completed" in row_str:
                self.log.info("---- Sucess in ----- ")
                self.log.info(row_str)
                result = True
                break
            elif "Exiting due to fatal error" in row_str:
                self.log.info("----ERROR -----")
                self.log.info(row_str)
                result = False
                break
            row_byte = await process.stdout.readline()
        process.terminate()
        return result

    async def kill_vpn_connection(self, process=None, type_vpn=None):
        self.log.info("kill vpn")
        if process:
            process.kill()
            await process.wait()
        else:
            retry_flg = True
            counter = 0
            while retry_flg:
                cmd = f'echo {self.password} | sudo -S killall '
                if type_vpn == "openvpn":
                    cmd += "openvpn"
                else:
                    cmd += "ss-local"
                process = await asyncio.create_subprocess_shell(cmd,
                                                                stdout=asyncio.subprocess.PIPE,
                                                                stderr=asyncio.subprocess.PIPE)
                stdout, stderr = await process.communicate()
                if process.returncode == 1:
                    retry_flg = False
                await asyncio.sleep(1)
                counter += 1
                print(f"counter: {counter}")


if __name__ == "__main__":
    db = DataBase()
    password = os.getenv("SUPER_ROOT_PASSWORD")
    vpn_checker = VPN_Checker(password, db)
    file_ = '/home/vintello/PycharmProjects/openvpn_checker/keys/ss/81.28.6.186.ss'
    file_ = '/home/vintello/PycharmProjects/openvpn_checker/keys/ss/168.119.57.171.ss'
    #file_ = '/home/vintello/PycharmProjects/openvpn_checker/keys/ss/217.12.223.190.ss'
    asyncio.run(vpn_checker.check_ss(file_))