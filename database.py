import aiosqlite
from typing import AsyncGenerator

from config import settings
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import declarative_base, sessionmaker
import logging

logger = logging.getLogger("dataBase")

engine = create_async_engine(settings.SQLALCHEMY_DATABASE_URL)
Base = declarative_base()
async_session = sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False, autocommit=False
)


async def init_db():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def get_session() -> AsyncGenerator[AsyncSession, None]:
    async with async_session() as session:
        yield session

class DataBase:
    def __init__(self, path_to_db="main.db"):
        self.path_to_db = path_to_db

    async def execute(self, sql: str, parameters: tuple = None, fetchone=False, fetchall=False, commit=False):
        try:
            async with aiosqlite.connect(self.path_to_db) as db:
                try:
                    if not parameters:
                        parameters = ()
                    data = None
                    cursor = await db.cursor()
                    await cursor.execute(sql, parameters)
                    if "INSERT" in sql:
                        data = cursor.lastrowid

                    if commit:
                        await db.commit()

                    if fetchone:
                        data = await cursor.fetchone()
                    if fetchall:
                        data = await cursor.fetchall()
                except Exception as ex:
                    logger.exception("выполнение запроса")
                # await cursor.close()
                # await db.close()
                return data
        except Exception as ex:
            print(ex)

    async def create_tables_by_default(self):
        # full_path -
        # delete_flg -
        sql = """
        CREATE TABLE IF NOT EXISTS config_files(
        id integer primary key,
        full_path text,
        delete_flg BOOLEAN default 0,
        date_add TEXT,
        type_files TEXT
        )
        """
        res = await self.execute(sql=sql, commit=True)
        # interval - интрвал повторения в секундах
        # name - имя периодической задачи
        # date_exec - дата последнего віполнения задачи в формате YYYY-MM-DD HH:MM:SS
        sql = """
                CREATE TABLE IF NOT EXISTS periodic_task(
                id integer primary key,
                name text,
                date_start text,
                date_finish text
                )
                """
        await self.execute(sql=sql, commit=True)
        sql = """
                        CREATE TABLE IF NOT EXISTS validated(
                        id integer primary key,
                        link_config_files_id integer,
                        link_periodic_task_id integer,
                        date_exec text,
                        result_validate text,
                        reasone_error text,
                        FOREIGN KEY(link_config_files_id) REFERENCES config_files(id),
                        FOREIGN KEY(link_periodic_task_id) REFERENCES periodic_task(id)
                        )
                        """
        await self.execute(sql=sql, commit=True)
        try:
            sql = """CREATE INDEX config_files_index_name on config_files (full_path)"""
            await self.execute(sql=sql, commit=True)
        except Exception as ex:
            pass
        try:
            sql = """CREATE INDEX periodic_task_index_1 on periodic_task (date_start)"""
            await self.execute(sql=sql, commit=True)
        except Exception as ex:
            pass
        try:
            sql = """CREATE INDEX periodic_task_index_2 on periodic_task (name)"""
            await self.execute(sql=sql, commit=True)
        except Exception as ex:
            pass
        try:
            sql = """CREATE INDEX validated_index_1 on validated (link_config_files_id, link_periodic_task_id, date_exec)"""
            await self.execute(sql=sql, commit=True)
        except Exception as ex:
            pass