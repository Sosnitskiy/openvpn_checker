from aiogram import types
#from aiogram.types.inline_keyboard_markup import InlineKeyboardMarkup, InlineKeyboardButton

kb_main = [
    [
            types.KeyboardButton(text="Последние сканы"),
            types.KeyboardButton(text="Cтатистика"),
            types.KeyboardButton(text="Доступность"),
    ]
]
kb_ovpn = [
        [
            types.KeyboardButton(text="Последний скан OVPN"),
            types.KeyboardButton(text="Cтатистика проверки по OVPN"),
            types.KeyboardButton(text="Выполнить проверку OVPN")
        ],
    ]

kb_ss = [
        [
            types.KeyboardButton(text="Последний скан SS"),
            types.KeyboardButton(text="Cтатистика проверки по SS"),
            types.KeyboardButton(text="Выполнить проверку SS")
        ],
    ]


kb_repeat_check_error_ss = [
#  [
#    types.InlineKeyboardButton(text="ff1", callback_data='button1'),
#    types.InlineKeyboardButton(text="ff2", callback_data='button2')
#  ],
  [
    types.InlineKeyboardButton(text="Повторно проверить не прошедших проверку", callback_data='button_check_error_ss')
  ]
]
keyboard_repeat_check_error_ss = types.InlineKeyboardMarkup(inline_keyboard=kb_repeat_check_error_ss)

kb_repeat_check_error_ovpn = [
  [
    types.InlineKeyboardButton(text="Повторно проверить не прошедших проверку", callback_data='button_check_error_ovpn')
  ]
]
keyboard_repeat_check_error_ovpn = types.InlineKeyboardMarkup(inline_keyboard=kb_repeat_check_error_ovpn)